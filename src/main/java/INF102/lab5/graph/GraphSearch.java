package INF102.lab5.graph;

import java.util.HashSet;
import java.util.Set;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

  private IGraph<V> graph;

  public GraphSearch(IGraph<V> graph) {
    this.graph = graph;
  }

  @Override
  public boolean connected(V u, V v) {
    // If either u or v doesn't exist in the graph, they can't be connected.
    if (graph.hasNode(v) == false || graph.hasNode(u) == false) {
      return false;
    }

    // Use a set to keep track of visited nodes during the traversal.
    Set<V> nodesFound = new HashSet<>();

    // Start the recursive traversal from node u.
    return depthFirstSearch(u, v, nodesFound);
  }

  private boolean depthFirstSearch(V current, V target, Set<V> nodesFound) {
    // If the current node is equal to the target, we found a path.
    if (current.equals(target)) {
      return true;
    }

    // Mark the current node as visited.
    nodesFound.add(current);

    // Check the neighbors of the current node.
    for (V neighbor : graph.getNeighbourhood(current)) {
      // If the neighbor hasn't been visited, recursively search from there.
      if (!nodesFound.contains(neighbor)) {
        if (depthFirstSearch(neighbor, target, nodesFound)) {
          return true;
        }
      }
    }

    // If we exhaust all neighbors and still haven't found the target, return false.
    return false;
  }
}
